<?php 
     require_once 'server/class/usuario.php';
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="lib/css/bootstrap.min.css">
    <link rel="stylesheet" href="lib/css/aos.css">
    <link rel="stylesheet" href="lib/css/all.min.css">

    

    <!--Css da pagina-->
    
    <link rel="stylesheet" href="lib/css/breakpoint.css">

    <link rel="shortcut icon" href="img/favicon.svg" type="image/x-icon">
    

    <title>BreakPoint</title>
</head>
<body>
    
    <header>

        <nav class="navbar navbar-expand-md shadow">
            <div class="container-fluid">

                <a class="navbar-brand" href="index.php">
                    <img src="img/brandwhite.svg" alt="brand">
                </a>
        
                <button class="navbar-toggler" type="button" onclick="openNav()">
                    <img src="img/menu.svg" alt="menu-icon">
                </button>
        
              
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
        
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="index.php">Home</a>
                        </li>
        
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Artigos <i class="fas fa-chevron-down"></i>
                            </a>
        
                            <div class="dropdown-menu border-0 shadow" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="games.php">Games</a>
                                <a class="dropdown-item" href="tecnologia.php">Tecnologia</a>
                            </div>
        
                        </li>
    
                        <li class="nav-item">
                            <a class="nav-link disabled" href="podcast.php" tabindex="-1" aria-disabled="true">Podcast</a>
                        </li>
    
                        <li class="nav-item">
                            <a class="nav-link" href="comunidade.php">Comunidade</a>
                        </li>
                    </ul>
    
                    
                    <a class="btn-login btn-login-white ml-3" href="login.php">Login</a>

                </div>
            </div>
            
        </nav>

        <div class="sidenav">
            <div class="container">

                <span class="fechar my-3" onclick="closeNav()"><i class="fas fa-chevron-right"></i></span>
             
                    <div class="mb-5">
                        <a href="index.php" class="link">Home</a>
    
                        <a data-toggle="collapse" href="#dropdown" class="link">Artigos <i class="fas fa-chevron-down"></i></a>
                            <div class="collapse multi-collapse" id="dropdown">
                                <a href="games.php" class="link">Games</a>
                                <a href="tecnologia.php" class="link">Tecnologia</a>
                            </div>
    
                        <a href="index.php" class="link link-disabled">Podcast</a>
                        <a href="comunidade.php" class="link">Comunidade</a>
    
                    </div>
    
                <div class="row justify-content-center">
                    <div class="col-8">
                        <a class="btn-login btn-login-white d-block text-center" href="login.php">Login</a>
                    </div>
                </div>
                    
           
            </div>
        </div>

    </header>

    <article id="about">

        <section class="banner">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-10 col-sm-10 col-12" data-aos="fade-right" data-aos-duration="1500">
                        <h3>Bem-Vindo</h3>
                        <h3>Gamer</h3>
                        <h4 class="mt-3 pb-5">Este espaço está sendo desenvolvido para você.</h4>
                    </div>
                </div>
            </div>
        </section>

        <!--o que?-->
        
        <section class="mt-5">

            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-10 col-lg-8 col-md-8 col-sm-10 col-12">
                        <h1>O que é o BreakPoint</h1>
                    </div>
                </div>

                <div class="row justify-content-center my-5">
                    <div class="col-xl-10 col-lg-10 col-md-10 col-sm-12 col-12">
                       
                        <p>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                            Quos placeat iste soluta laudantium a nisi consectetur 
                            sunt tenetur deserunt ad dolores, ipsam sapiente quas 
                            omnis voluptates amet. Fugit, quaerat quas!
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                            Quos placeat iste soluta laudantium a nisi consectetur 
                            sunt tenetur deserunt ad dolores, ipsam sapiente quas 
                            omnis voluptates amet. Fugit, quaerat quas!
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                            Quos placeat iste soluta laudantium a nisi consectetur 
                            sunt tenetur deserunt ad dolores, ipsam sapiente quas 
                            omnis voluptates amet. Fugit, quaerat quas!
                        </p>

                    </div>
                </div>
            </div>

        </section>

        <!--Objetivo-->
        <section id="objetivo" class="mb-5">
            <div class="container">
                <h1>Nossos objetivos</h1>

                <div class="row justify-content-center">
                    <div class="col-xl-8 col-lg-9 col-md-10 col-sm-12 col-12 way">
                        <div class="jumbotron" data-aos="zoom-in" data-aos-duration="1000">
                            <h2 class="mb-4"><i class="fas fa-newspaper"></i> Artigos</h2>
                            <p>
                                Para aqueles que possuem um interesse ainda maior nesse universo 
                                rico e grandioso, aqui encontrará abrigo, acolhimento, quase que 
                                um abraço bem apertado dos nossos escritores, dedicados em trazer 
                                o “fino do fino", das curiosidades e histórias mais interessantes 
                                e diversas desse mundo.
                            </p>
                        </div>
                    </div>
                </div>

                
                <div class="row justify-content-center my-5">  
                    <div class="col-xl-8 col-lg-9 col-md-10 col-sm-12 col-12 way">
                        <div class="jumbotron" data-aos="zoom-in" data-aos-duration="1000">
                            <h2 class="mb-4"><i class="fas fa-podcast"></i> Podcast</h2>
                            <p>
                                Para aqueles que não tem tempo a perder, mas não querem deixar 
                                de se manter informados e atualizados sobre as diversas novidades 
                                do mundo dos games e tecnologia.
                            </p>
                        </div>
                    </div>
                </div>

                
                <div class="row justify-content-center">
                    <div class="col-xl-8 col-lg-9 col-md-10 col-sm-12 col-12">
                        <div class="jumbotron" data-aos="zoom-in" data-aos-duration="1000">
                            <h2 class="mb-4"><i class="fas fa-users"></i> Comunidade</h2>
                            <p>
                                Nossa comunidade foi criada para aproximar cada vez mais os usuários do 
                                site e de outras plataformas que possuam a marca do Breakpoint. Através 
                                do nosso grupo do Discord, vocês terão a oportunidade de interagir com 
                                os desenvolvedores e até contribuir com suas ideias.
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </section>
      
    </article>

    <footer>
        <div class="container pt-4 pb-3">
            <h4 class="copyright"><i class="far fa-copyright"></i> copyright 2020 - BreakPoint | Todos os direitos reservados. <a href=""></a></h4>
        </div>
    </footer>

    <div class="dark-mode">
        <img src="img/theme.svg" classs="img-fluid darklink">
    </div>

    <div class="modal bd-modal-md" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header"></div>

                <div class="modal-body py-5">

                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-xl-10 col-lg-10 col-md-10 col-sm-12 col-12">
                                <h1 class="modal-title">Hey Gamer!</h1>

                                <p class="modal-text my-4">
                                    Seja bem-vindo ao breakpoint, este espaço está sendo desenvolvido 
                                    para você. Sendo assim, o site ainda está em período de Beta, há 
                                    muitas coisas que ainda serão reveladas no futuro. Então fique ligado!
                                </p>

                                <button class="btn-login btn-login-orange d-block mx-auto" id="btn-modal">Valeu!</button>

                            </div>
                        </div>
                    </div>

                </div>

                <div class="modal-footer"></div>
            </div>
        </div>
    </div>


    <script src="lib/js/jquery.slim.min.js"></script>
    <script src="lib/js/popper.min.js"></script>
    <script src="lib/js/bootstrap.min.js"></script>
    <script src="lib/js/aos.js"></script>
    <script src="lib/js/all.min.js"></script>
    <script src="lib/script/arquivo.js"></script>
    <script src="lib/script/popup.js"></script>
    <script src="lib/script/theme.js"></script>
</body>
</html>