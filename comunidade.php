<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="lib/css/bootstrap.min.css">
    <link rel="stylesheet" href="lib/css/aos.css">
    <link rel="stylesheet" href="lib/css/all.min.css">

    
    <!--Css da pagina-->
    
    <link rel="stylesheet" href="lib/css/breakpoint.css">

    <link rel="shortcut icon" href="img/favicon.svg" type="image/x-icon">
    

    <title>Comunidade</title>
</head>
<body>
    
    <header>

        <nav class="navbar navbar-expand-md shadow">
            <div class="container-fluid">

                <a class="navbar-brand" href="index.php">
                    <img src="img/brandwhite.svg" alt="brand">
                </a>
        
                <button class="navbar-toggler" type="button" onclick="openNav()">
                    <img src="img/menu.svg" alt="menu-icon">
                </button>
        
              
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
        
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="index.php">Home</a>
                        </li>
        
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Artigos <i class="fas fa-chevron-down"></i>
                            </a>
        
                            <div class="dropdown-menu border-0 shadow" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="games.php">Games</a>
                                <a class="dropdown-item" href="tecnologia.php">Tecnologia</a>
                            </div>
        
                        </li>
    
                        <li class="nav-item">
                            <a class="nav-link disabled" href="podcast.php" tabindex="-1" aria-disabled="true">Podcast</a>
                        </li>
    
                        <li class="nav-item">
                            <a class="nav-link" href="#">Comunidade</a>
                        </li>
                    </ul>
    
                    
                    <a class="btn-login btn-login-white ml-3" href="login.php">Login</a>
                    
        
                </div>
            </div>
            
        </nav>

        <div class="sidenav">
            <div class="container">

                <span class="fechar my-3" onclick="closeNav()"><i class="fas fa-chevron-right"></i></span>
             
                    <div class="mb-5">
                        <a href="index.php" class="link">Home</a>
    
                        <a data-toggle="collapse" href="#dropdown" class="link">Artigos <i class="fas fa-chevron-down"></i></a>
                            <div class="collapse multi-collapse" id="dropdown">
                                <a href="games.php" class="link">Games</a>
                                <a href="tecnologia.php" class="link">Tecnologia</a>
                            </div>
    
                        <a href="index.php" class="link link-disabled">Podcast</a>
                        <a href="comunidade.php" class="link">Comunidade</a>
    
                    </div>
    
                <div class="row justify-content-center">
                    <div class="col-8">
                        <a class="btn-login btn-login-white d-block text-center" href="login.php">Login</a>
                    </div>
                </div>
                    
           
            </div>
        </div>

    </header>

    <article>
        <section class="comunidade">
            <div class="container">

                <div class="row justify-content-around pt-5">
                    <div class="col-xl-6 col-lg-6 col-md-10 col-sm-12 col-12 align-self-center">
                        <h1>BreakPoint no Discord</h1>
                        <p class="my-5">
                            Lorem ipsum dolor sit amet consectetur, adipisicing elit. 
                            Reiciendis voluptate beatae accusantium asperiores a, saepe 
                            cumque, laudantium consequatur voluptatibus temporibus tempora 
                            nesciunt magnam iure omnis autem blanditiis, quidem porro nam.
                        </p>

                        <a href="http://" target="_blank" rel="noopener noreferrer" class="btn-login btn-login-orange">
                            <i class="fab fa-discord"></i> Entrar 
                        </a>
                    </div>

                    <div class="col-xl-5 col-lg-5 col-md-10 col-sm-12 col-12 mt-5">
                        <img src="img/discord.svg" class="img-fluid d-block mx-auto" data-aos="zoom-in" data-aos-duration="1000">
                    </div>
                </div>
                
            </div>

           

        </section>
    </article>

    <footer>
        <div class="container pt-4 pb-3">
            <h4 class="copyright"><i class="far fa-copyright"></i> copyright 2020 - BreakPoint | Todos os direitos reservados. <a href=""></a></h4>
        </div>
    </footer>

    <div class="dark-mode">
        <img src="img/theme.svg" classs="img-fluid darklink">
    </div>


    <script src="lib/js/jquery.slim.min.js"></script>
    <script src="lib/js/popper.min.js"></script>
    <script src="lib/js/bootstrap.min.js"></script>
    <script src="lib/js/aos.js"></script>
    <script src="lib/js/all.min.js"></script>
    <script src="lib/script/arquivo.js"></script>
    <script src="lib/script/theme.js"></script>
</body>
</html>