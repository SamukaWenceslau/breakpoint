# BreakPoint - Press Start!

<img src="/breakpoint/src/view/img/banner.png">

### O que é? 

* BreakPoint é um projeto desenvolvido por uma equipe de 5 estudantes
de ciência da computação que, tem o desafio de criar um site o qual a
pauta são os games (jogos eletrônicos) e a tecnologia (tanto na area de 
desenvolvimento de games, quanto no desenvolvimento de programas/softwares). 


### Objetivos:

- [x] Desenvolver um modelo funcional e colocar-lo no ar;

- [x] Adquirir conhecimento e experiencia no desenvolvimento web;

- [x] Integrar novos componentes e novas funcionalidades, conforme o aprendizado e o desenvolvimento da equipe;

- [x] Trabalhar com as principais plataformas, sendo elas: Twitch, Youtube, Spotify, Instagram, Facebook e discord.


### Tecnologias (iniciais):

| FRONT-END | BACK-END |
|-----------|----------|
| Html, Css | PHP      |
| Bootstrap | MySQL    |
|JavaScript |          |


### Equipe: 

[Matheus_de_Biace_Torres](https://github.com/Xyberevyk)
[Mateus_Jesus](https://github.com/Mateusisalreadytaken)
[Matheus_Costa](https://github.com/thegodofsouls)
[Rodrick](https://github.com/rodrigoscruz)
[SamukaWenceslau](https://github.com/SamukaWenceslau)

















 
