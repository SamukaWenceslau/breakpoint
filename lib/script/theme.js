let darkMode = localStorage.getItem("darkMode");
const btnDark = document.querySelector(".dark-mode");

// Ativa o dark mode
const enableDarkMode = () => {
    document.body.classList.add("switchTheme");

    localStorage.setItem("darkMode", "enabled");
};

// Desativa o dark mode
const disabledDarkMode = () => {
    document.body.classList.remove("switchTheme");

    localStorage.setItem("darkMode", null);
};

if(darkMode === "enabled") {
    enableDarkMode();
}


btnDark.addEventListener("click", () => {
    darkMode = localStorage.getItem("darkMode");

    if(darkMode !== "enabled") {
        enableDarkMode();

    }else {
        disabledDarkMode();
    }
});