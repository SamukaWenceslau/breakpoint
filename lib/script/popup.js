const modal = document.querySelector(".modal");
const button = document.getElementById("btn-modal");
let popup = localStorage.getItem("status");

const enabledPopUp = () => {
    modal.style.opacity = "100%";

    localStorage.setItem("status", null);
};

const disabledPopUp = () => {
    modal.style.opacity = "0";
    modal.style.display = "none";

    localStorage.setItem("status", "already");
};

if(popup === null) {
   enabledPopUp();
}


window.setTimeout(()=>{
    
    if(popup !== "already") {
        enabledPopUp();
        console.log(popup);
    }else{
        disabledPopUp();
        console.log(popup);
    }
}, 1500);


button.addEventListener("click", ()=>{
    modal.style.opacity = "0";
    modal.style.display = "none";
    localStorage.setItem("status", "already");
});