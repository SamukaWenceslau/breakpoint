<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="../lib/css/bootstrap.min.css">
    <link rel="stylesheet" href="../lib/css/aos.css">
    <link rel="stylesheet" href="../lib/css/all.min.css">
    <link href="https://fonts.googleapis.com/css?family=Bebas+Neue|Heebo:800,400|Lexend+Deca|Poppins:800,300,500|Raleway:900|Staatliches&display=swap" rel="stylesheet">
    

    <!--Css da pagina-->

    <link rel="stylesheet" href="../lib/style/tools.css">
    

    <title>Artigos - podcast</title>
</head>
<body>

    <header>
        <nav class="navbar navbar-expand-md shadow">
            <div class="container-fluid">
    
                <a class="navbar-brand" href="../index.php">
                    <img src="../img/brand.svg" alt="brand">
                </a>
        
                <button class="navbar-toggler" type="button" onclick="openNav()">
                    <img src="../img/menu.svg" alt="menu-icon">
                </button>
        
              
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
        
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="../index.php">Home</a>
                        </li>
        
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Artigos <i class="fas fa-chevron-down"></i>
                            </a>
        
                            <div class="dropdown-menu border-0 shadow" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="../games.php">Games</a>
                                <a class="dropdown-item" href="../tecnologia.php">Tecnologia</a>
                            </div>
        
                        </li>
    
                        <li class="nav-item">
                            <a class="nav-link disabled" href="../podcast.php" tabindex="-1" aria-disabled="true">Podcast</a>
                        </li>
    
                        <li class="nav-item">
                            <a class="nav-link" href="../comunidade.php">Comunidade</a>
                        </li>
                    </ul>
      
                    <a class="btn-login btn-login-black ml-4" href="../login.php">Login</a>
                
                </div>
    
            </div>
        </nav>

        <div class="sidenav">
            <div class="container">
    
                <span class="fechar my-3" onclick="closeNav()"><i class="fas fa-chevron-right"></i></span>
             
                <div class="mb-5">
                    <a href="../index.php" class="link">Home</a>
    
                    <a data-toggle="collapse" href="#dropdown" class="link">Artigos <i class="fas fa-chevron-down"></i></a>
                    <div class="collapse multi-collapse" id="dropdown">
                        <a href="../games.php" class="link">Games</a>
                        <a href="../tecnologia.php" class="link">Tecnologia</a>
                    </div>
    
                    <a href="../podcast.php" class="link">Podcast</a>
                    <a href="../comunidade.php" class="link">Comunidade</a>
    
                </div>
    
                <a class="btn-login btn-login-white ml-4" href="../login.php">Login</a>
           
            </div>
        </div>
    </header>
    
    <article>
        <section class="modelo">

            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-8">
                        <h1>Titulo do artigo aqui</h1>

                        <h4 class="my-4">Subtitulo do artigo aqui</h4>

                        <h5><i class="fas fa-user-edit"></i> JahnkerX</h5>

                        <h5><i class="far fa-calendar-alt"></i> Data da publicação - 00/00/0000</h5>

                        <img src="../img/background.jpg" class="img-fluid my-5" alt="">

                        <p>
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. 
                            Perspiciatis quis repudiandae rem illo ipsum, nam hic 
                            repellat culpa earum cumque sed numquam! Enim fugiat 
                            doloremque recusandae ratione temporibus eum quo!
                        </p>

                        <p>
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. 
                            Perspiciatis quis repudiandae rem illo ipsum, nam hic 
                            repellat culpa earum cumque sed numquam! Enim fugiat 
                            doloremque recusandae ratione temporibus eum quo!
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. 
                            Perspiciatis quis repudiandae rem illo ipsum, nam hic 
                            repellat culpa earum cumque sed numquam! Enim fugiat 
                            doloremque recusandae ratione temporibus eum quo!
                        </p>

                        <p>
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. 
                            Perspiciatis quis repudiandae rem illo ipsum, nam hic 
                            repellat culpa earum cumque sed numquam! Enim fugiat 
                            doloremque recusandae ratione temporibus eum quo!
                        </p>
                    </div>
                </div>
            </div>


            <div class="container">
                <div class="jumbotron mt-5"></div>
            </div>

        </section>
    </article>


    <script src="../lib/js/jquery.slim.min.js"></script>
    <script src="../lib/js/popper.min.js"></script>
    <script src="../lib/js/bootstrap.min.js"></script>
    <script src="../lib/js/all.min.js"></script>
    <script src="../lib/js/aos.js"></script>
    <script src="../lib/script/arquivo.js"></script>
</body>
</html>