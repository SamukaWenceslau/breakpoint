<?php
    require_once 'class/usuario.php';
    require_once 'class/conexao.php';
   
    //instanciou a classe
    $u = new Usuario($pdo);

    if(isset($_POST['usuario'])) 
    {    
        $nome_usuario = strip_tags($_POST['usuario']); 
        $senha = strip_tags($_POST['senha']);

        if(!empty($nome_usuario) && !empty($senha)) //verificaçao se os campos email e senha nao estao vazios
        {
               
        if($u->logar($nome_usuario, $senha)) //caso o usuario digitou o email e senha correto
        {
            $u->getIp();
            //header("Location: logado.php"); 
            echo '<script language="JavaScript">location.href="workspace.php"</script>';
        }
        else //caso o usuario tenha digitado o email ou senha errada
        {
            echo "<script>alert('Usuario ou senha incorretos!!');</script>";
            echo '<script language="JavaScript">location.href="login.php"</script>';
        }
    }
        else
        {
            echo "<script>alert('preencha os campos');</script>";
            echo '<script language="JavaScript">location.href="login.php"</script>';
        }
                
    }
?>