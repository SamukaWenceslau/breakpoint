<?php
    class Usuario
    {
     //atributo de conexao
     private $pdo;
 
     public function __construct($pdo)
     {
         $this->pdo = $pdo;
     } 
    //método responsavel por verificar se existem usuarios cadastrados e logar
    public function logar($nome_usuario, $senha)
    {
       
        /*verificar se o email e senha estao cadastrados, se sim*/
        $sql = $this->pdo->prepare("SELECT id_usuario FROM usuario WHERE nome_usuario = :u AND senha = :s");
        $sql->bindValue(":u", $nome_usuario);
        $sql->bindValue(":s", md5($senha));
        $sql->execute();
   
        // $resp = $this->pdo->query("SELECT * FROM usuarios WHERE email = '{$email}' AND senha = '{$senha}'");
        //var_dump($resp->fetch());die;
     

        if($sql->rowCount() > 0) //verificando se houve resposta na consulta
        {
        
        $_SESSION['logar'] = null; //faz com que o usuario nao consiga acessar antes de logar digitando o email e senha
         
        //entrar no sistema criando uma (sessao)
        $dado = $sql->fetch(); //transforma o retorno da query em array com os nomes das colunas
         
        session_start();  //iniciando a sessao
        $_SESSION['logar'] = $dado['id_usuario']; //armazena o id do usuario na sessao logar = nome da sessao criada.
         
        return true;  //logado com sucesso
        
        }
        
        else
        {
        
        return false; //erro ao logar.
        
        }
    }
    
    //método responsavel por pegar o IP da maquina
    public function getIp() 
    {
        //pegando o ip da maquina

        $ip = $_SERVER['REMOTE_ADDR']; //guardando o ip na váriavel
        
        $dado = $_SESSION['logar']; 

        $sql = "UPDATE usuario SET ip = :ip WHERE id_usuario = :id_usuario";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(":ip", $ip);
        $sql->bindValue(":id_usuario", $dado);
        $sql->execute();

        if($sql->rowCount() > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    //método responsavel por impedir que dois usuarios consiga se logar na mesma conta com IPs diferentes
    public function existeIpLogado($dado, $ip) 
    {
       $dado = $_SESSION['logar'];
       $ip = $_SERVER['REMOTE_ADDR'];

       $sql = "SELECT * FROM usuario WHERE id_usuario = :id_usuario AND ip = :ip";
       $sql = $this->pdo->prepare($sql);
       $sql->bindValue(":id_usuario", $dado);
       $sql->bindValue(":ip", $ip);
       $sql->execute();
       
       //verificaçao se tem algum usuario logado na mesma conta com IP diferente
       if($sql->rowCount() == 0) 
       {
          return false; //desloga automaticamente
       }
       else 
       {
          return true; //se mantém logado na conta
       }
    }
    
    //método responsavel por pegar o nome de usuario
    public function nomeUsuario()
    {
        $sql = $this->pdo->query("SELECT * FROM usuario");
        
        //verificação se existe usuario cadastrado no banco
        if($sql->rowCount() > 0)
        {
            return $sql->fetchAll();
        }
        else
        {
            return array();
        }
    }
}

?>