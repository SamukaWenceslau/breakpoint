<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/aos.css">
    <link rel="stylesheet" href="../css/all.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Lexend+Deca&family=Quicksand:wght@400;500&family=Roboto:wght@100;300;400;500;900&display=swap" rel="stylesheet">
    

    <link rel="stylesheet" href="../style/tools.css">
    

    <title>Login</title>
</head>
<body>
    
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-4">
                
                <h2 class="text-center"><i class="far fa-user-circle"></i> Login</h2>

                <form action="../modal/logar_submit.php" method="POST">
                    <div class="form-group mt-5">
                        <label for="exampleInputEmail1">Usuario</label>
                        <input type="text" name="usuario" class="form-control">
                    </div>

                    <div class="form-group mb-5">
                        <label for="exampleInputPassword1">Senha</label>
                        <input type="password" name="senha" class="form-control">
                    </div>

                    <button type="submit" class="btn-login btn-login-green d-block mx-auto">Enviar <i class="fas fa-paper-plane"></i></button>
                </form>

            </div>
        </div>
    </div>






    <script src="../js/jquery.slim.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/aos.js"></script>
    <script src="../js/all.min.js"></script>
    <script src="../script/arquivo.js"></script>
    <script src="../script/scroll.js"></script>
</body>
</html>