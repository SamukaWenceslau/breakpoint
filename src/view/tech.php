<?php
     require_once '../modal/class/textos.php'; //chamando o arquivo da classe texto onde contem a conexao com banco
     require_once '../modal/class/conexao.php';

     $t = new Textos($pdo); //instanciou a classe Textos 
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/aos.css">
    <link rel="stylesheet" href="../css/all.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Lexend+Deca&family=Quicksand:wght@400;500&family=Roboto:wght@100;300;400;500;900&display=swap" rel="stylesheet">
    
    
    <!--Css da pagina-->

    <link rel="stylesheet" href="../style/tools.css">
    

    <title>Artigos - tecnologia</title>
</head>
<body>

    <header class="fixed-top">
        <nav class="navbar navbar-expand-md">
            <div class="container-fluid">

                <a class="navbar-brand" href="#">
                    <img src="../img/brand.svg" alt="brand">
                </a>
        
                <button class="navbar-toggler" type="button" onclick="openNav()">
                    <img src="../img/menu.svg" alt="menu-icon">
                </button>
        
              
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
        
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="sobre.php">Sobre</a>
                        </li>
        
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Artigos <i class="fas fa-chevron-down"></i>
                            </a>
        
                            <div class="dropdown-menu border-0 shadow" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="games.php">Games</a>
                                <a class="dropdown-item" href="tech.php">Tecnologia</a>
                            </div>
        
                        </li>
    
                        <li class="nav-item">
                            <a class="nav-link" href="podcast.php" tabindex="-1" aria-disabled="true">Podcast</a>
                        </li>
    
                        <li class="nav-item">
                            <a class="nav-link" href="#">Comunidade</a>
                        </li>
                    </ul>
    
                    
                    <button class="btn-login btn-login-black ml-3" data-toggle="modal" data-target="#modallogin">Login</button>
                    
        
                </div>
            </div>
            
        </nav>
    
        <div class="progress green"></div>

        <div class="sidenav">
            <div class="container">
    
                <span class="fechar my-3" onclick="closeNav()"><i class="fas fa-chevron-right"></i></span>
             
                <div class="mb-5">
                    <a href="sobre.php" class="link">Sobre</a>
    
                    <a data-toggle="collapse" href="#dropdown" class="link">Artigos <i class="fas fa-chevron-down"></i></a>
                    <div class="collapse multi-collapse" id="dropdown">
                        <a href="games.php" class="link">Games</a>
                        <a href="tech.php" class="link">Tecnologia</a>
                    </div>
    
                    <a href="podcast.php" class="link">Podcast</a>
                    <a href="comunidade.php" class="link">Comunidade</a>
    
                </div>
    
                <button class="btn-login btn-login-white ml-4" data-toggle="modal" data-target="#modallogin">Login</button>
           
            </div>
        </div>
    </header>
    
    <section class="banner-tech">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-xl-5 col-lg-5 col-md-6 col-sm-8 col-10">

                    <div class="card">
                        <div class="card-body green">
                            <h1>Tecnologia</h1>
                        </div>
                    </div>
                
                    <h5 class="subtitle">Encontre artigos relacionados a games aqui.</h5>
                    <h3 class="text-center text-white"><i class="fas fa-chevron-down"></i></h3>
                </div>
    
            </div>
        </div>
    </section>

    <article>
        <div class="container-fluid">

            <div class="row justify-content-around my-5">
        
                <div class="col-xl-7 col-lg-7 col-md-10 col-sm-12 col-12">

                    <?php
                        $dados = $t->mostrarTextos();
                        foreach($dados as $item): 
                    ?>
                            
                    <div class="row justify-content-center my-5" >
        
                        <div class="col-xl-7 col-lg-6 col-md-6 col-sm-12 col-12 mb-4">
                            <img src="../img/background.jpg" class="img-fluid rounded-0">
                        </div>
        
                        <div class="col-xl-5 col-lg-6 col-md-6 col-sm-10 col-12 align-self-center">

                            <h5 class="tech">
                                 <?php echo $item["categoria"]; ?>  
                            </h5>

                            <h2><?php echo $item["titulo"]; ?></h2>

                            <p><?php echo $item["subtitulo"]; ?></p>

                            <h6><i class="far fa-calendar-alt"></i> Data da publicação -
                        
                                <?php 
                                    //convertendo data para padrao brasileiro
                                    $data = date('d/m/Y', strtotime($item['data']));
                                    echo $data; 
                                ?>  
                            </h6>

                        </div> 

                    </div>
        
                 
                    <?php 
                        endforeach;
                        echo $t->contarPaginas();
                    ?>
                    
                </div>

                <!--Propaganda-->
                <div class="col-xl-3 col-lg-3 d-none d-lg-block">
                    <?php require_once('propaganda.php');?>
                </div>

            </div>

        </div>

        <div class="container">
            <div class="jumbotron mt-5"></div>
        </div>

    </article>

                      

    <script src="../js/jquery.slim.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/all.min.js"></script>
    <script src="../js/aos.js"></script>
    <script src="../script/arquivo.js"></script>
    <script src="../script/scroll.js"></script>
</body>
</html>