<?php 
     require_once '../modal/class/usuario.php';
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/aos.css">
    <link rel="stylesheet" href="../css/all.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Lexend+Deca&family=Quicksand:wght@400;500&family=Roboto:wght@100;300;400;500;900&display=swap" rel="stylesheet">
    

    <!--Css da pagina-->
    

    <link rel="stylesheet" href="../style/tools.css">
    

    <title>Sobre</title>
</head>
<body>
    
    <header class="fixed-top">

        <nav class="navbar navbar-expand-md">
            <div class="container-fluid">

                <a class="navbar-brand" href="#">
                    <img src="../img/brand.svg" alt="brand">
                </a>
        
                <button class="navbar-toggler" type="button" onclick="openNav()">
                    <img src="../img/menu.svg" alt="menu-icon">
                </button>
        
              
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
        
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="sobre.php">Sobre</a>
                        </li>
        
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Artigos <i class="fas fa-chevron-down"></i>
                            </a>
        
                            <div class="dropdown-menu border-0 shadow" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="games.php">Games</a>
                                <a class="dropdown-item" href="tech.php">Tecnologia</a>
                            </div>
        
                        </li>
    
                        <li class="nav-item">
                            <a class="nav-link" href="podcast.php" tabindex="-1" aria-disabled="true">Podcast</a>
                        </li>
    
                        <li class="nav-item">
                            <a class="nav-link" href="#">Comunidade</a>
                        </li>
                    </ul>
    
                    
                    <a class="btn-login btn-login-black ml-3" href="../modal/login.php">Login</a>
                    
        
                </div>
            </div>
            
        </nav>

        <div class="progress dark"></div>
    
        <div class="sidenav">
            <div class="container">
    
                <span class="fechar my-3" onclick="closeNav()"><i class="fas fa-chevron-right"></i></span>
             
                <div class="mb-5">
                    <a href="sobre.php" class="link">Sobre</a>
    
                    <a data-toggle="collapse" href="#dropdown" class="link">Artigos <i class="fas fa-chevron-down"></i></a>
                    <div class="collapse multi-collapse" id="dropdown">
                        <a href="games.php" class="link">Games</a>
                        <a href="tech.php" class="link">Tecnologia</a>
                    </div>
    
                    <a href="podcast.php" class="link">Podcast</a>
                    <a href="comunidade.php" class="link">Comunidade</a>
    
                </div>
    
                <a class="btn-login btn-login-white ml-4" href="login.php">Login</a>
           
            </div>
        </div>

    </header>

    <article id="about">

        <!--o que?-->
        
        <section>

            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-6 col-lg-6 col-md-8 col-sm-10 col-12">
                        <h1>O que é o BreakPoint?</h1>
                    </div>
                </div>

                <div class="row justify-content-center my-5">
                    <div class="col-xl-10 col-lg-10 col-md-10 col-sm-12 col-12">
                       
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen></iframe>
                    </div>

                    </div>
                </div>
            </div>

        </section>

        <!--Objetivo-->
        <section id="objetivo">
            <div class="container">
                <h1>Nossos objetivos</h1>

                <div class="row justify-content-center mt-5">
                    <div class="col-xl-8 col-lg-8 col-md-10 col-sm-12 col-12 way">
                        <div class="jumbotron">
                            <h2 class="mb-4">Artigos</h2>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
                                Delectus reiciendis voluptatibus adipisci voluptatem voluptate 
                                ipsam atque recusandae unde tempora sequi repudiandae iure, 
                                architecto consequatur, corporis, ut et! Ex, ullam iure?
                            </p>
                        </div>
                    </div>
                </div>

                
                <div class="row justify-content-center my-5">  
                    <div class="col-xl-8 col-lg-8 col-md-10 col-sm-12 col-12 way">
                        <div class="jumbotron">
                            <h2 class="mb-4">Podcast</h2>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
                                Delectus reiciendis voluptatibus adipisci voluptatem voluptate 
                                ipsam atque recusandae unde tempora sequi repudiandae iure, 
                                architecto consequatur, corporis, ut et! Ex, ullam iure?
                            </p>
                        </div>
                    </div>
                </div>

                
                <div class="row justify-content-center">
                    <div class="col-xl-8 col-lg-8 col-md-10 col-sm-12 col-12">
                        <div class="jumbotron">
                            <h2 class="mb-4">Comunidade</h2>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
                                Delectus reiciendis voluptatibus adipisci voluptatem voluptate 
                                ipsam atque recusandae unde tempora sequi repudiandae iure, 
                                architecto consequatur, corporis, ut et! Ex, ullam iure?
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </section>
      
    </article>


    <script src="../js/jquery.slim.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/aos.js"></script>
    <script src="../js/all.min.js"></script>
    <script src="../script/arquivo.js"></script>
    <script src="../script/scroll.js"></script>
</body>
</html>