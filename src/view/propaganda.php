<div class="topics my-5">
    <ul class="list-group list-group-flush mt-5">
        <li class="list-group-item"><a href="#"><i class="fab fa-spotify"></i> Spotify</a></li>
        <li class="list-group-item"><a href="#"><i class="fab fa-twitch"></i> Twitch</a></li>
        <li class="list-group-item"><a href="#"><i class="fab fa-youtube"></i> YouTube</a></li>
        <li class="list-group-item"><a href="#"><i class="fab fa-instagram"></i> Instagram</a></li>
        <li class="list-group-item"><a href="#"><i class="fab fa-google-play"></i> Google Play</a></li>                 
    </ul>
</div>    