<?php
     require_once 'server/class/textos.php'; //chamando o arquivo da classe texto onde contem a conexao com banco
     require_once 'server/class/conexao.php';

     $t = new Textos($pdo); //instanciou a classe Textos 
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="lib/css/bootstrap.min.css">
    <link rel="stylesheet" href="lib/css/aos.css">
    <link rel="stylesheet" href="lib/css/all.min.css">
    

    <!--Css da pagina-->
    
    <link rel="stylesheet" href="lib/css/breakpoint.css">

    <link rel="shortcut icon" href="img/favicon.svg" type="image/x-icon">

    <title>Artigos - Games</title>
</head>
<body>


    
    <header>
        <nav class="navbar navbar-expand-md shadow">
            <div class="container-fluid">

                <a class="navbar-brand" href="index.php">
                    <img src="img/brandwhite.svg" alt="brand">
                </a>
        
                <button class="navbar-toggler" type="button" onclick="openNav()">
                    <img src="img/menu.svg" alt="menu-icon">
                </button>
        
              
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
        
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="index.php">Home</a>
                        </li>
        
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Artigos <i class="fas fa-chevron-down"></i>
                            </a>
        
                            <div class="dropdown-menu border-0 shadow" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="games.php">Games</a>
                                <a class="dropdown-item" href="tecnologia.php">Tecnologia</a>
                            </div>
        
                        </li>
    
                        <li class="nav-item">
                            <a class="nav-link disabled" href="podcast.php" tabindex="-1" aria-disabled="true">Podcast</a>
                        </li>
    
                        <li class="nav-item">
                            <a class="nav-link" href="comunidade.php">Comunidade</a>
                        </li>
                    </ul>
    
                    
                    <a class="btn-login btn-login-white ml-4" href="login.php">Login</a>
                    
        
                </div>
            </div>
            
        </nav>

    
        <div class="sidenav">
            <div class="container">
    
                <span class="fechar my-3" onclick="closeNav()"><i class="fas fa-chevron-right"></i></span>
             
                <div class="mb-5">
                    <a href="index.php" class="link">Home</a>
    
                    <a data-toggle="collapse" href="#dropdown" class="link">Artigos <i class="fas fa-chevron-down"></i></a>
                    <div class="collapse multi-collapse" id="dropdown">
                        <a href="games.php" class="link">Games</a>
                        <a href="tecnologia.php" class="link">Tecnologia</a>
                    </div>
    
                    <a href="index.php" class="link link-disabled">Podcast</a>
                    <a href="comunidade.php" class="link">Comunidade</a>
    
                </div>
    
                <a class="btn-login btn-login-white ml-4" href="login.php">Login</a>
           
            </div>
        </div>
    </header>

    
    <article>
        <div class="container-fluid">

            <div class="row justify-content-around my-5">
        
                <div class="col-xl-8 col-lg-7 col-md-10 col-sm-12 col-12">

                    <?php
                        $dados = $t->mostrarTextos();
                        foreach($dados as $item): 
                    ?>
                            
                    <div class="row justify-content-center my-5" >
                   
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-10 col-12 mb-4" data-aos="fade-right" data-aos-duration="1200">
                            <img src="<?php echo "server/upload/".$item['arquivo']; ?>" class="img-fluid img-border-games">
                        </div>
                   
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-10 col-12 align-self-center" data-aos="fade-right" data-aos-duration="1200">
                            
                            <h5 class="games">
                                <?php echo $item["categoria"]; ?>  
                            </h5>
                            
                        
                            <h1 class="my-2"><?php echo $item["titulo"]; ?></h1>

                            <h6 class="mt-3 mb-5"><i class="far fa-calendar-alt"></i> Data da publicação -
                                                
                                <?php 
                                    //convertendo data para padrao brasileiro
                                    $data = date('d/m/Y', strtotime($item['data']));
                                    echo $data; 
                                ?>  
                            </h6>


                        </div> 
                 
                    </div>
        
                 
                    <?php 
                        endforeach;
                        echo $t->contarPaginas();
                    ?>

                </div>

                <!--Propaganda-->
                <div class="col-xl-3 col-lg-3 d-none d-lg-block">
                    <?php require_once('propaganda.php');?>
                </div>

            </div>

        </div>

        <div class="container">
            <div class="jumbotron mt-5"></div>
        </div>
         
    </article>

    <footer>
        <div class="container pt-4 pb-3">
            <h4 class="copyright"><i class="far fa-copyright"></i> copyright 2020 - BreakPoint | Todos os direitos reservados. <a href=""></a></h4>
        </div>
    </footer>

    <div class="dark-mode">
        <img src="img/theme.svg" classs="img-fluid darklink">
    </div>





    <script src="lib/js/jquery.slim.min.js"></script>
    <script src="lib/js/popper.min.js"></script>
    <script src="lib/js/bootstrap.min.js"></script>
    <script src="lib/js/aos.js"></script>
    <script src="lib/js/all.min.js"></script>
    <script src="lib/script/arquivo.js"></script>
    <script src="lib/script/theme.js"></script>
</body>
</html>