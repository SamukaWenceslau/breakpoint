<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="lib/css/bootstrap.min.css">
    <link rel="stylesheet" href="lib/css/aos.css">
    <link rel="stylesheet" href="lib/css/all.min.css">
    
    
    <!--Css da pagina-->

    <link rel="stylesheet" href="lib/css/breakpoint.css">

    
    <link rel="shortcut icon" href="img/favicon.svg" type="image/x-icon">

    <title>Artigos - podcast</title>
</head>
<body>

    <header>
        <nav class="navbar navbar-expand-md">
            <div class="container-fluid">
    
                <a class="navbar-brand" href="index.php">
                    <img src="img/brandwhite.svg" alt="brand">
                </a>
        
                <button class="navbar-toggler" type="button" onclick="openNav()">
                    <img src="img/menu.svg" alt="menu-icon">
                </button>
        
              
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
        
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="index.php">Home</a>
                        </li>
        
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Artigos <i class="fas fa-chevron-down"></i>
                            </a>
        
                            <div class="dropdown-menu border-0 shadow" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="games.php">Games</a>
                                <a class="dropdown-item" href="tecnologia.php">Tecnologia</a>
                            </div>
        
                        </li>
    
                        <li class="nav-item">
                            <a class="nav-link" href="podcast.php" tabindex="-1" aria-disabled="true">Podcast</a>
                        </li>
    
                        <li class="nav-item">
                            <a class="nav-link" href="#">Comunidade</a>
                        </li>
                    </ul>
      
                    <a class="btn-login btn-login-white ml-4" href="login.php">Login</a>
                
                </div>
    
            </div>
        </nav>

        <div class="sidenav">
            <div class="container">
    
                <span class="fechar my-3" onclick="closeNav()"><i class="fas fa-chevron-right"></i></span>
             
                <div class="mb-5">
                    <a href="index.php" class="link">Home</a>
    
                    <a data-toggle="collapse" href="#dropdown" class="link">Artigos <i class="fas fa-chevron-down"></i></a>
                    <div class="collapse multi-collapse" id="dropdown">
                        <a href="games.php" class="link">Games</a>
                        <a href="tecnologia.php" class="link">Tecnologia</a>
                    </div>
    
                    <a href="index.php" class="link link-disabled">Podcast</a>
                    <a href="comunidade.php" class="link">Comunidade</a>
    
                </div>
    
                <a class="btn-login btn-login-white ml-4" href="login.php">Login</a>
           
            </div>
        </div>
    </header>
    

    <section class="banner-podcast">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-xl-5 col-lg-4 col-md-5 col-sm-8 col-10">

                    <div class="card">
                        <div class="card-body blue">
                            <h1>Podcast</h1>
                        </div>
                    </div>
                
                </div>
    
            </div>
        </div>
    </section>


    <script src="lib/js/jquery.slim.min.js"></script>
    <script src="lib/js/popper.min.js"></script>
    <script src="lib/js/bootstrap.min.js"></script>
    <script src="lib/js/all.min.js"></script>
    <script src="lib/js/aos.js"></script>
    <script src="lib/script/arquivo.js"></script>
</body>
</html>