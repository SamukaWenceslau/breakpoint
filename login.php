<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="lib/css/bootstrap.min.css">
    <link rel="stylesheet" href="lib/css/aos.css">
    <link rel="stylesheet" href="lib/css/all.min.css">

    

    <link rel="stylesheet" href="lib/css/breakpoint.css">

    <link rel="shortcut icon" href="img/favicon.svg" type="image/x-icon">
    

    <title>Login</title>
</head>
<body>
    
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-8">
                
                <h2 class="text-center"><i class="far fa-user-circle"></i> Login</h2>

                <form action="server/logar_submit.php" method="POST">
                    <div class="form-group mt-5">
                        <label for="exampleInputEmail1">Usuario</label>
                        <input type="text" name="usuario" class="form-control">
                    </div>

                    <div class="form-group mb-5">
                        <label for="exampleInputPassword1">Senha</label>
                        <input type="password" name="senha" class="form-control">
                    </div>

                    <button type="submit" class="btn-login btn-login-orange d-block mx-auto">Enviar <i class="fas fa-paper-plane"></i></button>
                </form>

            </div>
        </div>
    </div>

   

    <script src="lib/js/jquery.slim.min.js"></script>
    <script src="lib/js/popper.min.js"></script>
    <script src="lib/js/bootstrap.min.js"></script>
    <script src="lib/js/aos.js"></script>
    <script src="lib/js/all.min.js"></script>
    <script src="lib/script/arquivo.js"></script>
    <script src="lib/script/scroll.js"></script>
    <script src="lib/script/theme.js"></script>
</body>
</html>