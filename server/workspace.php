<?php
require_once 'class/textos.php'; //chamando o arquivo da classe texto onde contem a conexao com banco
require_once 'class/usuario.php';
require_once 'class/conexao.php';

$t = new Textos($pdo); //instanciou a classe Textos
$u = new Usuario($pdo); //instanciou a classe usuario

if(isset($_GET["id_texto"])) 
      { 

    	$id_texto = $_GET["id_texto"]; //pega o id de texto e salva na variavel

    	$info = $t->getInfo($id_texto);

      }   

?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link rel="stylesheet" href="../lib/css/bootstrap.min.css">
    <link rel="stylesheet" href="../lib/css/aos.css">
    <link rel="stylesheet" href="../lib/css/all.min.css">
    <link href="https://fonts.googleapis.com/css?family=Bebas+Neue|Heebo:800|Lexend+Deca|Poppins:800,400|Raleway:900|Staatliches&display=swap" rel="stylesheet">
    
    
    
    
    <!--Css da pagina-->
    
    <link rel="stylesheet" href="../lib/style/workspace.css">
    <link rel="stylesheet" href="../lib/style/buttons.css">
    
    <link rel="shortcut icon" href="../img/favicon.svg" type="image/x-icon">
    
    
    <title>WorkSpace</title>
</head>
<body>
    
    <?php
    session_start(); //abrir a sessao
    
    //fazer uma verificaçao se a sessao existe
    if(!isset($_SESSION['logar'])) //caso nao existir
    {
        //header("Location: logar.php"); //será reencaminhada para a tela de logar
        echo '<script language="JavaScript">location.href="../login.php"</script>';
        
        exit; //para nao executar nada além dessa linha
    } 
    else
    {
        $dado = $_SESSION['logar'];
        $ip = $_SERVER['REMOTE_ADDR'];
        if(!$u->existeIpLogado($dado, $ip)) 
        {
            //header("Location: logar.php");
            echo '<script language="JavaScript">location.href="../login.php"</script>';
            exit;
        }
    }   
    ?>
    
    <aside>
        <div class="container-fluid">
            <img src="../img/brandwhite.svg" class="img-fluid d-block mx-auto" alt="">
            
            <div class="list-group list-group-flush mt-5" id="list-tab" role="tablist">
                <a class="list-group-item" id="list-view-list" data-toggle="list" href="#view" role="tab"><i class="fas fa-eye"></i> Visualize</a>
                <a class="list-group-item" id="list-dashboard-list" data-toggle="list" href="#dashboard" role="tab"><i class="fas fa-tachometer-alt"></i> Dashboard</a>
                <a class="list-group-item" id="list-create-list" data-toggle="list" href="#create" role="tab"><i class="fas fa-plus"></i> Create</a>
                <a class="list-group-item" id="list-update-list" data-toggle="list" href="#update" role="tab"><i class="fas fa-edit"></i> Update</a>
                <a class="list-group-item" href="sair.php"><i class="fas fa-sign-out-alt"></i> Sair</a>
            </div>
        </div>
    </aside>
    
    <section>
        <div class="container">
            
            <div class="row justify-content-center">
                <div class="col-10">
                    <div class="tab-content" id="nav-tabContent">
                        
                        <!--VISUALIZAR-->
                        <div class="tab-pane fade show active" id="view" role="tabpanel">
                            
                            <div class="row justify-content-end">
                                <div class="col-10">
                                    
                                    <?php
                                    $dados = $t->mostrarTextos();
                                    foreach($dados as $item): 
                                    ?>
                                    
                                    <div class="row justify-content-center my-5" >
                                        
                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-10 col-12 mb-4">
                                            <img src="<?php echo "upload/".$item['arquivo']; ?>" class="img-fluid rounded-0">
                                        </div>
                                        
                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-10 col-12 align-self-center">
                                            
                                            <h5 class="games">
                                                <?php echo $item["categoria"]; ?>  
                                            </h5>
                                            
                                            
                                            <h2><?php echo $item["titulo"]; ?></h2>
                                            
                                            
                                            <h6 class="mt-3 mb-5"><i class="far fa-calendar-alt"></i> Data da publicação -
                                                
                                                <?php 
                                                //convertendo data para padrao brasileiro
                                                $data = date('d/m/Y', strtotime($item['data']));
                                                echo $data; 
                                                ?>  
                                            </h6>
                                            
                                            <a class="btn-login btn-login-black" href="workspace.php?id_texto=<?php echo $item['id_texto']; ?>" >Editar <i class="fas fa-pen"></i></a>
                                            
                                            <a class="btn-login btn-login-black" href="excluir.php?id_texto=<?php echo $item['id_texto']; ?>">
                                                Excluir <i class="fas fa-trash"></i>
                                            </a>
                                            
                                        </div> 
                                        
                                    </div>
                                    
                                    
                                    <?php 
                                    endforeach;
                                    echo $t->contarPaginas();
                                    ?>
                                    
                                </div>
                                
                            </div>
                            
                        </div>
                        
                        <!--DASHBOARD-->
                        <div class="tab-pane fade" id="dashboard" role="tabpanel">
                            <div class="row justify-content-end mb-5">
                                
                                <div class="col-10">
                                    <h2 class="my-4">Publicações</h2>
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row justify-content-around">
                                                <div class="col-3">
                                                    <h1>300</h1>
                                                    <p>Games</p>
                                                </div>
                                                
                                                <div class="col-3">                                                  
                                                    <h1>1000</h1>
                                                    <p>Tecnologia</p>
                                                </div>
                                                
                                                <div class="col-3">
                                                    <h1>10</h1>
                                                    <p>Podcast</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-5">
                                    
                                    <h2 class="mt-5 mb-3">Redes socias</h2>
                                    <div class="card">
                                        <div class="card-body">
                                            <ul class="list-group list-group-flush">
                                                <li class="list-group-item"><i class="fab fa-discord"></i> 500 subscribe</li>
                                                <li class="list-group-item"><i class="fab fa-twitch"></i> 500 subscribe</li>
                                                <li class="list-group-item"><i class="fab fa-youtube"></i> 500 subscribe</li>
                                                <li class="list-group-item"><i class="fab fa-instagram"></i> 500 subscribe</li>
                                                <li class="list-group-item"><i class="fab fa-spotify"></i> 500 subscribe</li>
                                            </ul>
                                        </div>
                                    </div>
                                    
                                </div>
                                
                                <div class="col-5">
                                    
                                    <h2 class="mt-5 mb-3">Conta</h2>
                                    <div class="card">
                                        <div class="card-body py-3">
                                            
                                            <h1>JahnkerX</h1>
                                            <h1 class="py-4">300</h1>
                                            <p>Publicações</p>
                                        </div>
                                    </div>
                                    
                                </div>
                                
                            </div>
                        </div>
                        
                        <!--CREATE-->
                        <div class="tab-pane fade" id="create" role="tabpanel">
                            <div class="row justify-content-end">
                                <div class="col-10 mt-4">
                                    
                                   
                                            <form action="inserir_submit.php" method="POST" enctype="multipart/form-data">
                                                
                                                <div class="form-group">
                                                    <input type="text" name="titulo" class="form-control form-control-lg" placeholder="Titulo *">
                                                </div>
                                                
                                                <div class="form-group my-4">  
                                                    <input type="text" name="subtitulo" class="form-control form-control-lg" placeholder="Subtitulo *">
                                                </div>
                                                
                                                <div class="form-row">
                                                    <div class="form-group col-md-4"> 
                                                        <input type="text" name="autor" class="form-control form-control-lg" placeholder="Autor *">
                                                    </div>
                                                    
                                                    <div class="form-group col-md-4">
                                                        <select id="inputState" name="categoria" class="form-control form-control-lg">
                                                            <option selected>Categoria *</option>
                                                            <option value="Games">Games</option>
                                                            <option value="Tecnologia">Tecnologia</option>
                                                        </select>
                                                    </div>
                                                    
                                                    <div class="form-group col-md-4">
                                                        <input type="date" name="data" class="form-control form-control-lg" id="inputZip">
                                                    </div>
                                                    
                                                </div>
                                                
                                                <div class="form-group">
                                                    <textarea class="form-control my-3" name="conteudo" rows="10" placeholder="Texto *"></textarea>
                                                </div>
                                                
                                                <div class="form-group mb-5">
                                                    <input type="file" name="arquivo" class="form-control-file">
                                                </div>
                                                
                                                <button type="submit" class="btn-login btn-login-orange d-block mx-auto"  value="inserir">Enviar </button>
                                            </form>
                                            
                                        
                                    
                                    
                                </div>
                            </div>
                        </div>
                        
                        <!--UPDATE-->
                        <div class="tab-pane fade" id="update" role="tabpanel">
                            
                            <div class="row justify-content-end">
                                <div class="col-10">
                                    
                                            <form action="editar_submit.php" method="POST" enctype="multipart/form-data">
                                                
                                                <div class="form-group">
                                                    <input type="hidden" name="id_texto" class="form-control form-control-lg" value="<?php echo $info['id_texto']; ?>" />
                                                </div>
                                                
                                                <div class="form-group">
                                                    <input type="text" name="titulo" class="form-control form-control-lg" value="<?php echo $info['titulo']; ?>">
                                                </div>
                                                
                                                <div class="form-group my-4">  
                                                    <input type="text" name="subtitulo" class="form-control form-control-lg" value="<?php echo $info['subtitulo']; ?>">
                                                </div>
                                                
                                                <div class="form-row">
                                                    <div class="form-group col-md-4"> 
                                                        <input type="text" name="autor" class="form-control form-control-lg" value="<?php echo $info['autor']; ?>">
                                                    </div>
                                                    
                                                    <div class="form-group col-md-4">
                                                        <select name="categoria" class="form-control form-control-lg" value="<?php echo $info['categoria']; ?>">
                                                            <option selected>Categoria *</option>
                                                            <option value="Games">Games</option>
                                                            <option value="Tecnologia">Tecnologia</option>
                                                        </select>
                                                    </div>
                                                    
                                                    <div class="form-group col-md-4">
                                                        <input type="date" name="data" class="form-control form-control-lg" value="<?php echo $info['data']; ?>">
                                                    </div>
                                                    
                                                </div>
                                                
                                                <div class="form-group">
                                                    <textarea class="form-control my-3" name="conteudo" rows="10" value="<?php echo $info['conteudo']; ?>"></textarea>
                                                </div>
                                                
                                                <div class="form-group mb-5">
                                                    <input type="file" name="arquivo" class="form-control-file" value="<?php echo $info['arquivo']; ?>">
                                                </div>
                                                
                                                <button type="submit" class="btn-login btn-login-orange d-block mx-auto"  value="inserir">Enviar </button>
                                            </form>
                                               
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    
   
    <div class="dark-mode">
        <img src="../img/theme.svg" classs="img-fluid darklink">
    </div>
    
    
    <script src="../lib/js/jquery.slim.min.js"></script>
    <script src="../lib/js/popper.min.js"></script>
    <script src="../lib/js/bootstrap.min.js"></script>
    <script src="../lib/js/all.min.js"></script>
    <script src="../lib/js/aos.js"></script>
    <script src="../lib/script/theme.js"></script>
</body>
</html>