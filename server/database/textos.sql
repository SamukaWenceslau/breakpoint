-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 08-Jan-2020 às 23:57
-- Versão do servidor: 10.4.6-MariaDB
-- versão do PHP: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `breakpoint`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `textos`
--

CREATE TABLE `textos` (
  `id_texto` int(11) NOT NULL,
  `autor` varchar(100) NOT NULL,
  `titulo` varchar(230) NOT NULL,
  `subtitulo` mediumtext NOT NULL,
  `arquivo` varchar(255) NOT NULL,
  `conteudo` longtext NOT NULL,
  `data` date NOT NULL,
  `categoria` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `textos`
--

INSERT INTO `textos` (`id_texto`, `autor`, `titulo`, `subtitulo`, `arquivo`, `conteudo`, `data`, `categoria`) VALUES
(8, 'Matheus', 'Assassin\'s Creed', 'crenÃ§as e antepassados', '', 'historia entre assassinos e templÃ¡rios', '2020-01-04', 'Games'),
(10, 'Matheus', 'Rainbow Six', 'jogo dificil', '', 'muuuito difÃ­cil', '2020-01-05', 'Gamepac'),
(11, 'beltrano', 'Battlefield 4', 'tiro', '', 'jogo de guerra', '2020-01-05', 'Gamepac'),
(12, 'Ronaldinho', 'FIFA', 'jogo bosta', '', 'jogo onde ficam batendo uma bolinha', '2020-01-05', 'outros'),
(13, 'javeiros', 'Java ', 'linguagem bosta', '', 'linguagem de programaÃ§Ã£o bosta', '2020-01-05', 'outros'),
(14, 'vcxvxcvcz', 'xx xvxvxvxzcvxz', 'sdffaffasf', '', 'sfsadfdsafsadfg', '2020-01-05', 'outros');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `textos`
--
ALTER TABLE `textos`
  ADD PRIMARY KEY (`id_texto`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `textos`
--
ALTER TABLE `textos`
  MODIFY `id_texto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
