<?php

require_once '../server/class/textos.php';
require_once '../server/class/conexao.php';

$t = new Textos($pdo);

      if(isset($_GET["id_texto"])) 
      { 

    	$id_texto = $_GET["id_texto"]; //pega o id de texto e salva na variavel

    	$info = $t->getInfo($id_texto);

       if(empty($info['autor']) || empty($info['titulo']) || empty($info['subtitulo']) || empty($info['arquivo']) || empty($info['conteudo']) || empty($info['categoria']))
       {
           echo '<script language="JavaScript">location.href="logado.php"</script>';
    	   exit;
       }
       
      }      else //caso contrario
      { 
        echo '<script>alert("Enviado com sucesso!");</script>';
        echo '<script language="JavaScript">location.href="logado.php"</script>';
    	exit;
      }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>editar</title>
</head>
<body>
    <form action="editar_submit.php" method="POST" enctype="multipart/form-data">
    
    <input type="hidden" name="id_texto" value="<?php echo $info['id_texto']; ?>" />
    
    autor:<br>
    <input type="text" name="autor" value="<?php echo $info['autor']; ?>" /><br><br>
 
    título:<br>
    <input type="text" name="titulo" value="<?php echo $info['titulo']; ?>" /><br><br>
    
    subtitulo:<br>
    <input type="text" name="subtitulo" value="<?php echo $info['subtitulo']; ?>" /><br/><br/>

    conteudo:<br>
    <textarea name="conteudo" cols="30" rows="10" value="<?php echo $info['conteudo']; ?>"></textarea><br/>

    data:<br>
    <input type="date" name="data" value="<?php echo $info['data']; ?>" /><br><br>

    categoria:<br/>
    <select name="categoria" value="<?php echo $info['categoria']; ?>" required>
        <option value=""></option>
        <option value="Games">Games</option>
        <option value="Gamepac">Gamepac</option>
        <option value="outros">outros</option>
    </select><br><br>

    <input type="submit" value="inserir" />
     </form>
</body>
</html>