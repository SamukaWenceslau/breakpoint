<?php //abriu o php
      require_once 'class/textos.php';
      require_once 'class/conexao.php';
     
     $u = new Textos($pdo); //instanciou a classe textos

    //verificaçao se o campo titulo e conteudo estiverem preenchidos
    if(isset($_POST['autor']) && isset($_POST['titulo']) && isset($_POST['subtitulo']) && isset($_POST['conteudo']) && isset($_POST['categoria']) && isset($_FILES['arquivo'])) 
    {
       //pegar os campos do "name" do input no arquivo inserir.php e salvar nas variaveis
       $autor = strip_tags($_POST['autor']);
       $titulo = strip_tags($_POST['titulo']); 
       $subtitulo = strip_tags($_POST['subtitulo']);
       $conteudo = strip_tags($_POST['conteudo']);
       $data = strip_tags($_POST['data']);
       $categoria = strip_tags($_POST['categoria']);

       //detalhe: strtolower converte todas as extensoes para minusculo
       $extensao = strtolower(substr($_FILES['arquivo']['name'], -4)); //pega a extensão do arquivo
       $novo_nome = md5(time()).$extensao; // define o nome do arquivo mais a extensao
       $diretorio = "upload/"; //define o diretorio para onde enviaremos o arquivo
       
       move_uploaded_file($_FILES['arquivo']['tmp_name'], $diretorio.$novo_nome); //efetua o upload

        //verificaçao se os campos nao estiverem vazios
        if(!empty($autor) && !empty($titulo) && !empty($subtitulo) && !empty($conteudo) && !empty($categoria)) 
        {
            //se foi inserido
            if($u->inserirTextos($id_texto, $autor, $titulo, $subtitulo, $novo_nome, $conteudo, $data, $categoria)) 
             {
                
                 echo '<script>alert("Enviado com sucesso!");</script>';
                 echo '<script language="JavaScript">location.href="workspace.php"</script>';
             }
        }
        else //caso nao tenha preenchido todos os campos
        {
            echo "<script>alert('preencha os campos');</script>";
            echo '<script language="JavaScript">location.href="workspace.php"</script>';   
        }
     }
//fechou o php
?>