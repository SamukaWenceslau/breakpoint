<?php
     require_once 'class/textos.php';
     require_once 'class/conexao.php';

     $t = new Textos($pdo); //instanciou a classe Contato do contato.class.php
 
     //verificaçao se o id do texto existe para ser alterado
     if(!empty($_POST["id_texto"])) 
     { //verificar se tem id
        $autor = strip_tags($_POST['autor']);
        $titulo = strip_tags($_POST['titulo']); 
        $subtitulo = strip_tags($_POST['subtitulo']);
        $conteudo = strip_tags($_POST['conteudo']);
        $data = strip_tags($_POST['data']);
        $categoria = strip_tags($_POST['categoria']);

        //detalhe: strtolower converte todas as extensoes para minusculo
        $extensao = strtolower(substr($_FILES['arquivo']['name'], -4)); //pega a extensão do arquivo
        $novo_nome = md5(time()).$extensao; // define o nome do arquivo mais a extensao
        $diretorio = "upload/"; //define o diretorio para onde enviaremos o arquivo
        
        move_uploaded_file($_FILES['arquivo']['tmp_name'], $diretorio.$novo_nome); //efetua o upload

        $id_texto = strip_tags($_POST["id_texto"]); //pegar o id
          
          //verificaçao se os campos nao estiverem vazios
          if(!empty($_POST['autor']) && !empty($_POST['titulo']) && !empty($_POST['subtitulo']) && !empty($_POST['conteudo']) && !empty($_POST['categoria'])) 
            { 
                $t->editarTextos($id_texto, $autor, $titulo, $subtitulo, $novo_arquivo, $conteudo, $data, $categoria);
            }
 
            echo '<script language="JavaScript">location.href="workspace.php"</script>';
            
      }
?>      