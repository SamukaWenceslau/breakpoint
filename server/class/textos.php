<?php //abre o PHP
     class Textos //criou a classe texto
     {

     //atributo de conexao
     private $pdo;

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }
    
    //método criado responsavel por cadastrar mensagens
    public function inserirTextos($id_texto, $autor, $titulo, $subtitulo, $novo_nome, $conteudo, $data, $categoria)
    {

        //verificando se há usuario com determinado id existente cadasatrado
        $sql = $this->pdo->prepare("SELECT * FROM textos WHERE id_texto = :id_texto"); 
        $sql->bindValue(":id_texto", $id_texto); //salva o campo id na variavel local $id_texto
        $sql->execute(); //executa a consulta

        //caso possui um id existenta já cadastrado
        if($sql->rowCount() > 0) 
        {
             return false; //já está cadastrado
        }
        else 
        {
            //caso nao exista dados cadastrados
            
            $data = date("Y-m-d");  
            date('d/m/Y', strtotime($data));
            //insere os dados na tabela textos
            $sql = $this->pdo->prepare("INSERT INTO textos (autor, titulo, subtitulo, arquivo, conteudo, data, categoria) VALUES (:autor, :titulo, :subtitulo, :arquivo, :conteudo, :data, :categoria)"); 
            $sql->bindValue(":autor", $autor);
            $sql->bindValue(":titulo", $titulo);
            $sql->bindValue(":subtitulo", $subtitulo);
            $sql->bindValue(":arquivo", $novo_nome);
            $sql->bindValue(":conteudo", $conteudo);
            $sql->bindValue(":data", $data);
            $sql->bindValue(":categoria", $categoria);
            $sql->execute();

            return true; //o usuario foi cadastrado com sucesso
        }
    }
    
    //método reponsavel para listar os textos
    public function mostrarTextos()
    { 
        # para retornar 5 registros por paginas
        $qt_por_paginas = 10;
        $pg = 1;
        
        //verificaçao se esta preenchido o p na URL responsavel pela contagem de paginas
        if(isset($_GET['p']) && !empty($_GET['p']))
        {
            $pg = addslashes($_GET['p']);
        }
        
        $p = ($pg - 1) * $qt_por_paginas;

        // $p começa do 0 e vai até o registro 5
        $sql = $this->pdo->query("SELECT * FROM textos ORDER BY id_texto DESC LIMIT $p, $qt_por_paginas");
    
        //se existe algum texto cadastrado
        if($sql->rowCount() > 0) 
        {
            return $sql->fetchAll(); //retorna os registros mostrando-os na tela
        }
        else //caso contrário
        {
            return array(); //retorna um array vazio
        }

    }
    //método responsavel pela paginaçao
    public function contarPaginas() 
    {
          //iniciando a sessao
        
           /* 
          1. calcular a quantidade total de paginas.
          2. definir o $p
          3. fazer a consulta com LIMIT
        */
        $qt_por_paginas = 10; 
        $total = 0;
        $sql = "SELECT COUNT(*) as c FROM textos";
        $sql = $this->pdo->query($sql);
        $sql = $sql->fetch(); //fetch porque só existe um retorno nessa Query
        $total = $sql['c'];
        
        # pra saber quantidade de paginas no total
        
        //pegou o total e dividiu pela quantidade por pagina
        $paginas = $total / $qt_por_paginas;
    
        echo '<hr>';
        echo'<nav>'; 
        echo '<ul class="pagination pagination-lg justify-content-end">';
        for($q = 0; $q < $paginas; $q++)
        {
            //echo '<a href="./?p='.($q + 1).'">['.($q + 1).']</a>'; 
            echo'<li class="page-item"><a class="page-link" href="?p='.($q + 1).'">'.($q + 1).'</a></li>';
        }
        echo'</ul>';
        echo'</nav>';
            
    }

    //metodo reposnsavel por editar os textos
    public function editarTextos($id_texto, $autor, $titulo, $subtitulo, $novo_nome, $conteudo, $data, $categoria) 
    {
        $data = date("Y-m-d");  
        date('d/m/Y', strtotime($data));

        $sql = "UPDATE textos SET autor = :autor, titulo = :titulo, subtitulo = :subtitulo, arquivo = :arquivo, conteudo = :conteudo, 
        data = :data, categoria = :categoria WHERE id_texto = :id_texto";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(":autor", $autor);
        $sql->bindValue(":titulo", $titulo);
        $sql->bindValue(":subtitulo", $subtitulo);
        $sql->bindValue(":arquivo", $novo_nome);
        $sql->bindValue(":conteudo", $conteudo);
        $sql->bindValue(":data", $data);
        $sql->bindValue(":categoria", $categoria);
        $sql->bindValue(":id_texto", $id_texto);
        $sql->execute();
        
        return true;
    
    }
    
    //metodo de informaçao para identificar se existe o id do usuario
    //pegar informaçoes deu um contato especifico recebendo id
    public function getInfo($id_texto) 
    { 
        $sql = "SELECT * FROM textos WHERE id_texto = :id_texto";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(':id_texto', $id_texto);
        $sql->execute();
        
        if ($sql->rowCount() > 0) 
        {
            return $sql->fetch(); //pega um dado por vez
        } 
        else 
        {
            return array(); //se nao achar retorna um array vazio
        }
    
    }

    //metodo para excluir os textos
    public function excluirTextos($id_texto)
    {
        $sql = $this->pdo->prepare("DELETE FROM textos WHERE id_texto = :id_texto");
        $sql->bindValue(":id_texto", $id_texto);
        $sql->execute();
    }
}

?> 